package xyz.mizarc.physicalshops;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.mizarc.physicalshops.commands.TransferOwnershipCommand;
import xyz.mizarc.physicalshops.guimenu.MenuContainer;
import xyz.mizarc.physicalshops.guimenu.MenuInteraction;

import java.io.File;

public class PhysicalShops extends JavaPlugin {
    private Economy economy;
    private FileConfiguration config = getConfig();
    private ShopContainer shopContainer;
    private MenuContainer menuContainer;

    @Override
    public void onEnable() {
        getLogger().info("onEnable is called!");

        initialiseEconomy();
        shopContainer = new ShopContainer();
        menuContainer = new MenuContainer();



        config.addDefault("youAreAwesome", true);
        config.options().copyDefaults(true);
        saveConfig();

        File data = new File(getDataFolder(), "data");
        data.mkdir();

        getServer().getPluginManager().registerEvents(new ShopCreationListener(), this);
        getServer().getPluginManager().registerEvents(new ShopDestructionListener(), this);
        getServer().getPluginManager().registerEvents(new ShopUseListener(), this);
        getServer().getPluginManager().registerEvents(new MenuInteraction(), this);

        this.getCommand("transfershopowner").setExecutor(new TransferOwnershipCommand());

        shopContainer.loadSavedShops();
    }

    @Override
    public void onDisable() {
        getLogger().info("onDisable is called!");
    }

    public static PhysicalShops getPlugin() {
        return JavaPlugin.getPlugin(PhysicalShops.class);
    }

    public Economy getEconomy() {
        return economy;
    }

    public ShopContainer getShopContainer() {
        return shopContainer;
    }

    public MenuContainer getMenuContainer() {
        return menuContainer;
    }

    private boolean initialiseEconomy() {
        if (Bukkit.getPluginManager().getPlugin("Vault") == null) {
            return false;
        }

        RegisteredServiceProvider<Economy> service = getServer().getServicesManager().getRegistration(Economy.class);
        if (service == null) {
            return false;
        }

        economy = service.getProvider();
        return economy != null;
    }
}
