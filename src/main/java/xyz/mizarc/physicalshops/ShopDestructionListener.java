package xyz.mizarc.physicalshops;

import org.bukkit.Location;
import org.bukkit.block.Barrel;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.ShulkerBox;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ShopDestructionListener implements Listener {
    private final BlockFace[] blockSides = new BlockFace[] {
        BlockFace.NORTH,
        BlockFace.SOUTH,
        BlockFace.WEST,
        BlockFace.EAST
    };

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        // Check if block broken is wall sign
        if (event.getBlock().getBlockData() instanceof WallSign) {
            removeShop(event.getBlock().getLocation());
        }

        // Check if block broken is chest and check if any of the sides are a wall sign
        else if (!(event.getBlock().getState() instanceof Chest || event.getBlock().getState() instanceof Barrel ||
                event.getBlock().getState() instanceof ShulkerBox)) {
            for (BlockFace side : blockSides) {
                if (!(event.getBlock().getRelative(side).getBlockData() instanceof WallSign)) {
                    continue;
                }

                WallSign sign = (WallSign) event.getBlock().getRelative(side).getBlockData();
                if (sign.getFacing().equals(side.getOppositeFace())){
                    continue;
                }

                removeShop(event.getBlock().getRelative(side).getLocation());
            }
        }
    }

    private void removeShop(Location location) {
        ShopContainer shops = PhysicalShops.getPlugin().getShopContainer();
        Shop shop = shops.getShop(location);
        if (shop == null) {
            return;
        }

        PhysicalShops.getPlugin().getShopContainer().removeShop(shop);
        DatabaseManager database = new DatabaseManager();
        database.removeStoreEntry(location);
        database.closeConnection();
    }
}
