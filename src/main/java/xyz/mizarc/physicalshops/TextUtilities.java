package xyz.mizarc.physicalshops;

import org.bukkit.Bukkit;

public class TextUtilities {

    public static String capitaliseWords(String string) {
        // Split string into individual words
        String[] words = string.split("\\s");

        // Capitalise first letter of each word
        StringBuilder capitalised = new StringBuilder();
        for(String word : words) {
            String first = word.substring(0, 1);
            String remaining = word.substring(1);
            capitalised.append(first.toUpperCase()).append(remaining.toLowerCase()).append(" ");
        }

        return capitalised.toString().trim();
    }

    public static String removeUnderscores(String string) {
        String split = string.replaceAll("_", " ");

        return split.trim();
    }

    public static String formatFloat(Float number) {
        if (number % 1 != 0) {
            Bukkit.broadcastMessage(Float.toString(number % 1));
            return number.toString();
        }

        Integer intNumber = (int) Math.floor(number);
        return intNumber.toString();
    }
}
