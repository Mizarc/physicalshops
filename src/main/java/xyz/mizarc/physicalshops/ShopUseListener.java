package xyz.mizarc.physicalshops;

import org.bukkit.Bukkit;
import org.bukkit.block.*;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import xyz.mizarc.physicalshops.guimenu.menus.ShopMenu;

public class ShopUseListener implements Listener {

    @EventHandler
    public void onSignRightClick(PlayerInteractEvent event) {
        // Stop if action is not right clicking a wall sign with an item in hand
        if (event.getClickedBlock() == null) {
            return;
        }
        if (!(event.getClickedBlock().getBlockData() instanceof WallSign)) {
            return;
        }
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        // Assign sign related variables
        Sign sign = (Sign) event.getClickedBlock().getState();
        WallSign wallSign = (WallSign) event.getClickedBlock().getBlockData();
        BlockFace signFace = wallSign.getFacing().getOppositeFace();
        Block attachedBlock = event.getClickedBlock().getRelative(signFace);

        // Stop if attached block is not a chest or barrel
        if(!(attachedBlock.getState() instanceof Chest || attachedBlock.getState() instanceof Barrel ||
                attachedBlock.getState() instanceof ShulkerBox)) {
            return;
        }

        ShopContainer shopContainer = PhysicalShops.getPlugin().getShopContainer();
        Shop shop = shopContainer.getShop(sign.getLocation());
        Container container = (Container) attachedBlock.getState();
        Inventory shopStorage = container.getInventory();

        if (shop != null) {
            ShopMenu shopGUI = new ShopMenu(event.getPlayer(), shop, shopStorage);
        }

    }
}
