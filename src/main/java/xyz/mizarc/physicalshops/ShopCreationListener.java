package xyz.mizarc.physicalshops;

import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ShopCreationListener implements Listener {

    private Map<String, Shop> playerShopSetup;
    private Map<String, Boolean> priceFlag;
    private Map<String, Boolean> quantityFlag;
    private Map<String, Float> price;
    private Map<String, Integer> quantity;
    private Map<String, Sign> sign;

    public ShopCreationListener() {
        playerShopSetup = new HashMap<>();
        priceFlag = new HashMap<>();
        quantityFlag = new HashMap<>();
        price = new HashMap<>();
        quantity = new HashMap<>();
        sign = new HashMap<>();
    }

    @EventHandler
    public void onSignRightClick(PlayerInteractEvent event) {
        // Stop if action is not right clicking a wall sign with an item in hand
        if (event.getClickedBlock() == null) {
            return;
        }
        if (!(event.getClickedBlock().getBlockData() instanceof WallSign)) {
            return;
        }
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        if (event.getItem() == null) {
            return;
        }

        // Assign sign related variables
        Sign sign = (Sign) event.getClickedBlock().getState();
        WallSign wallSign = (WallSign) event.getClickedBlock().getBlockData();
        BlockFace signFace = wallSign.getFacing().getOppositeFace();
        Block attachedBlock = event.getClickedBlock().getRelative(signFace);

        // Stop if attached block is not a chest
        if (!(attachedBlock.getState() instanceof Chest || attachedBlock.getState() instanceof Barrel ||
        attachedBlock.getState() instanceof ShulkerBox)) {
            return;
        }

        // Stop if text is on other than the first line
        String[] signLines = sign.getLines();
        for (int i = signLines.length - 1; i > 0; i--) {
            if (!signLines[i].isEmpty()) {
                return;
            }
        }

        // Set if shop is in buy or sell mode
        int shopMode = 0;
        String shopModeText = signLines[0];
        if (shopModeText.equals("[Buy]")) {
            shopMode = 0;
        } else if (shopModeText.equals("[Sell]")) {
            shopMode = 1;
        }

        // Set shop data object variables of currently known variables
        Shop shopData = new Shop();
        shopData.setOwner(event.getPlayer().getUniqueId().toString());
        shopData.setMode(shopMode);
        shopData.setWorld(event.getPlayer().getWorld().getName());
        shopData.setXPos(event.getClickedBlock().getX());
        shopData.setYPos(event.getClickedBlock().getY());
        shopData.setZPos(event.getClickedBlock().getZ());

        ItemStack item = event.getItem().clone();
        item.setAmount(1);
        shopData.setItem(item);

        playerShopSetup.put(event.getPlayer().getUniqueId().toString(), shopData);
        this.sign.put(event.getPlayer().getUniqueId().toString(), sign);

        anvilPricePrompt(event.getPlayer());
    }

    @EventHandler
    public void onAnvilLeave(InventoryCloseEvent event) {
        // Stop if inventory is not anvil
        if (event.getInventory().getType() != InventoryType.ANVIL) {
            return;
        }

        // Stop if shop creation is not currently in progress
        String playerId = event.getPlayer().getUniqueId().toString();
        if (!playerShopSetup.containsKey(playerId)) {
            return;
        }

        // Cancel shop creation if no price or quantity has been specified
        if ((priceFlag.get(playerId) != null && price.get(playerId) == null) ||
                (quantityFlag.get(playerId) != null && quantity.get(playerId) == null)) {
            if (price.get(playerId) != null) {
                price.remove(playerId);
            }
            if (quantity.get(playerId) != null) {
                quantity.remove(playerId);
            }
            if (priceFlag.get(playerId) != null) {
                priceFlag.remove(playerId);
            }
            if (quantityFlag.get(playerId) != null) {
                quantityFlag.remove(playerId);
            }
            if (playerShopSetup.get(playerId) != null) {
                playerShopSetup.remove(playerId);
            }

            return;
        }

        // Set shop price on anvil close
        Shop shopData = playerShopSetup.get(playerId);
        if (priceFlag.get(playerId) != null) {
            shopData.setPrice(price.get(playerId));
        }

        // Set quantity price on anvil close
        if (quantityFlag.get(playerId) != null) {
            shopData.setQuantity(quantity.get(playerId));
        }

        // Open quantity prompt if no quantity has been selected yet
        if (quantityFlag.get(playerId) == null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    anvilQuantityPrompt((Player) event.getPlayer());
                }
            }.runTaskLater(PhysicalShops.getPlugin(), 1);

            return;
        }

        PhysicalShops.getPlugin().getShopContainer().addShop(shopData);
        DatabaseManager database = new DatabaseManager();
        database.addStoreEntry(shopData);
        database.closeConnection();

        addSignShopInfo(sign.get(playerId), playerShopSetup.get(playerId));

        playerShopSetup.remove(playerId);
        price.remove(playerId);
        quantity.remove(playerId);
        priceFlag.remove(playerId);
        quantityFlag.remove(playerId);
        sign.remove(playerId);
    }

    private void anvilPricePrompt(Player player) {
        ItemStack goldNugget = new ItemStack(Material.GOLD_NUGGET);

        new AnvilGUI.Builder()
                .title("Input Sell Price")
                .itemLeft(goldNugget)
                .text("0")
                .onComplete((playerA, text) -> {
                    for (;;) {
                        try {
                            price.put(playerA.getUniqueId().toString(), Float.parseFloat(text));
                            return AnvilGUI.Response.close();
                        } catch (NumberFormatException e) {
                            return AnvilGUI.Response.text("0");
                        }
                    }
                })
                .plugin(PhysicalShops.getPlugin())
                .open(player);

        priceFlag.put(player.getUniqueId().toString(), true);
    }

    private void anvilQuantityPrompt(Player player) {
        ItemStack goldNugget = new ItemStack(Material.GOLD_NUGGET);

        new AnvilGUI.Builder()
                .title("Input Quantity")
                .itemLeft(goldNugget)
                .text("0")
                .onComplete((playerA, text) -> {
                    for (;;) {
                        try {
                            quantity.put(playerA.getUniqueId().toString(), Integer.parseInt(text));
                            return AnvilGUI.Response.close();
                        } catch (NumberFormatException e) {
                            return AnvilGUI.Response.text("0");
                        }
                    }
                })
                .plugin(PhysicalShops.getPlugin())
                .open(player);

        quantityFlag.put(player.getUniqueId().toString(), true);
    }

    private void addSignShopInfo(Sign sign, Shop shop) {
        String playerName = Bukkit.getPlayer(UUID.fromString(shop.getOwner())).getName();
        ItemStack item = shop.getItem();

        // Set text to Buying/Selling depending on mode
        String modeText;
        if (shop.getMode() == 0) {
            modeText = ChatColor.GREEN + "Buying";
        } else {
            modeText = ChatColor.AQUA + "Selling";
        }

        // Set quantity text amount if greater than one
        String quantityText;
        if (shop.getQuantity() < 1) {
            quantityText = " Each";
        } else {
            quantityText = shop.getQuantity() + " for ";
        }

        sign.setLine(0, playerName);
        sign.setLine(1,  modeText);
        sign.setLine(2, ChatColor.GOLD + TextUtilities.capitaliseWords(
                TextUtilities.removeUnderscores(item.getType().name())));
        sign.setLine(3, quantityText + PhysicalShops.getPlugin().getEconomy().format(shop.getPrice()));
        sign.update();
    }
}
