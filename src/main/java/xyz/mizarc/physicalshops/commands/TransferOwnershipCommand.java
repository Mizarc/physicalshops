package xyz.mizarc.physicalshops.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.mizarc.physicalshops.PhysicalShops;
import xyz.mizarc.physicalshops.Shop;
import xyz.mizarc.physicalshops.ShopContainer;

public class TransferOwnershipCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = ((Player) sender).getPlayer();
            Block block = player.getTargetBlock(null, 5);

            if (!(block.getState() instanceof Sign)) {
                player.sendMessage("Run this command while looking at a shop sign");
                return false;
            }

            Sign sign = (Sign) block.getState();
            Location signLocation = sign.getLocation();
            ShopContainer container = PhysicalShops.getPlugin().getShopContainer();
            Shop shop = container.getShop(signLocation);
            if (shop == null) {
                player.sendMessage("Run this command while looking at a shop sign");
                return false;
            }

            if (!shop.getOwner().equals(player.getUniqueId().toString())) {
                player.sendMessage("That's not your shop");
                return false;
            }

            OfflinePlayer newOwner = Bukkit.getOfflinePlayerIfCached(args[0]);
            if (newOwner == null) {
                player.sendMessage("That's not a valid player name");
                return false;
            }

            shop.setOwner(newOwner.getUniqueId().toString());
            shop.setSignOwner();
            return true;
        }

        return true;
    }
}
