package xyz.mizarc.physicalshops;

import org.bukkit.Location;

import java.util.HashSet;
import java.util.Set;

public class ShopContainer {
    private Set<Shop> loadedShops = new HashSet<>();

    public void loadSavedShops() {
        DatabaseManager database = new DatabaseManager();
        loadedShops = database.getAllShops();
    }

    public Shop getShop(Location location) {
        for (Shop shop : loadedShops) {
            if (location.getBlockX() == shop.getXPos() && location.getBlockY() == shop.getYPos() &&
                    location.getBlockZ() == shop.getZPos()) {
                return shop;
            }
        }
        return null;
    }

    public void addShop(Shop shop) {
        loadedShops.add(shop);
    }

    public void removeShop(Shop shop) {
        loadedShops.remove(shop);
    }
}
