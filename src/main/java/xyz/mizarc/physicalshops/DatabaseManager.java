package xyz.mizarc.physicalshops;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class DatabaseManager {
    private Connection connection = null;

    public DatabaseManager() {
        // Create database and table if it doesn't exist
        if (connection == null)
        {
            openConnection();
            createTable();
        }
    }

    public void openConnection() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:sqlite:" + PhysicalShops.getPlugin().getDataFolder() + "/store.db");
        } catch (SQLException error) {
            error.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException error) {
            error.printStackTrace();
        }
    }

    public Shop getShop(String world, int xPos, int yPos, int zPos) {
        String sqlSelect = "SELECT * FROM shops WHERE world=? AND x=? AND y=? AND z=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlSelect);
            statement.setString(1, world);
            statement.setInt(2, xPos);
            statement.setInt(3, yPos);
            statement.setInt(4, zPos);
            ResultSet resultSet = statement.executeQuery();

            Shop shop = new Shop();
            while (resultSet.next()) {
                shop = new Shop(
                    resultSet.getString(1),
                    resultSet.getInt(3),
                    resultSet.getString(4),
                    resultSet.getFloat(5),
                    resultSet.getInt(6),
                    resultSet.getString(7),
                    resultSet.getInt(8),
                    resultSet.getInt(9),
                    resultSet.getInt(10));
            }
            return shop;

        } catch (SQLException error) {
            error.printStackTrace();
        }

        return null;
    }

    public Set<Shop> getAllShops() {
        String sqlSelect = "SELECT * FROM shops;";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlSelect);
            ResultSet resultSet = statement.executeQuery();

            Set<Shop> shops = new HashSet<>();
            while (resultSet.next()) {
                shops.add(new Shop(
                        resultSet.getString(1),
                        resultSet.getInt(3),
                        resultSet.getString(4),
                        resultSet.getFloat(5),
                        resultSet.getInt(6),
                        resultSet.getString(7),
                        resultSet.getInt(8),
                        resultSet.getInt(9),
                        resultSet.getInt(10)));
            }
            return shops;

        } catch (SQLException error) {
            error.printStackTrace();
        }

        return null;
    }

    public void addStoreEntry(Shop shopData) {
        String sqlInsert =
                "INSERT INTO shops (owner, mode, item, price, quantity, world, x, y, z) VALUES (?,?,?,?,?,?,?,?,?);";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlInsert);
            statement.setString(1, shopData.getOwner());
            statement.setInt(2, shopData.getMode());
            statement.setString(3, shopData.getItemSerialised());
            statement.setFloat(4, shopData.getPrice());
            statement.setInt(5, shopData.getQuantity());
            statement.setString(6, shopData.getWorld());
            statement.setInt(7, shopData.getXPos());
            statement.setInt(8, shopData.getYPos());
            statement.setInt(9, shopData.getZPos());
            statement.executeUpdate();
            statement.close();

        } catch (SQLException error) {
            error.printStackTrace();
        }
    }

    public void removeStoreEntry(Location shopLocation) {
        String sqlRemove = "DELETE FROM shops WHERE x=? AND y=? AND z=?;";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlRemove);
            statement.setInt(1, shopLocation.getBlockX());
            statement.setInt(2, shopLocation.getBlockY());
            statement.setInt(3, shopLocation.getBlockZ());
            statement.executeUpdate();
            statement.close();

        } catch (SQLException error) {
            error.printStackTrace();
        }
    }

    public void changeShopOwnerEntry(String owner, Location shopLocation) {
        String sqlUpdate = "UPDATE shops SET owner=? WHERE world=? AND x=? AND y=? AND z=?;";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlUpdate);
            statement.setString(1, owner);
            statement.setString(2, shopLocation.getWorld().getName());
            statement.setInt(3, shopLocation.getBlockX());
            statement.setInt(4, shopLocation.getBlockY());
            statement.setInt(5, shopLocation.getBlockZ());
            statement.executeUpdate();
            statement.close();

        } catch (SQLException error) {
            error.printStackTrace();
        }
    }

    private void createTable() {
        // Create table if it doesn't exit
        String sqlCreate = "CREATE TABLE IF NOT EXISTS shops (owner TEXT NOT NULL, managers TEXT, mode INT NOT NULL, " +
                "item TEXT NOT NULL, price FLOAT NOT NULL, quantity INT NOT NULL, world TEXT NOT NULL, " +
                "x INTEGER NOT NULL, y INTEGER NOT NULL, z INTEGER NOT NULL);";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlCreate);
            statement.executeUpdate();
            statement.close();

        } catch (SQLException error) {
            error.printStackTrace();
        }
    }
}
