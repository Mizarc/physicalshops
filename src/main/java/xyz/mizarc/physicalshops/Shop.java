package xyz.mizarc.physicalshops;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class Shop {
    private String owner;
    private Integer mode;
    private String item;
    private float price;
    private int quantity;
    private String world;
    private int xPos;
    private int yPos;
    private int zPos;

    public Shop() {}

    public Shop(String owner, Integer mode, String item, float price, int quantity, String world,
                int xPos, int yPos, int zPos) {
        this.owner = owner;
        this.mode = mode;
        this.item = item;
        this.price = price;
        this.quantity = quantity;
        this.world = world;
        this.xPos = xPos;
        this.yPos = yPos;
        this.zPos = zPos;
    }

    public String getOwner() { return owner; }
    public Integer getMode() { return mode; }

    public ItemStack getItem() {
        YamlConfiguration yamlCfg = new YamlConfiguration();
        try {
            yamlCfg.loadFromString(item);
            return yamlCfg.getItemStack("item");

        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getItemSerialised() { return item; }
    public float getPrice() { return price; }
    public int getQuantity() { return quantity; }
    public String getWorld() { return world; }
    public int getXPos() { return xPos; }
    public int getYPos() { return yPos; }
    public int getZPos() { return zPos; }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public void setItem(ItemStack item) {
        YamlConfiguration yamlCfg = new YamlConfiguration();
        yamlCfg.set("item", item);
        this.item = yamlCfg.saveToString();
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public void setXPos(double xPos) {
        this.xPos = (int) xPos;
    }

    public void setYPos(double yPos) {
        this.yPos = (int) yPos;
    }

    public void setZPos(double zPos) {
        this.zPos = (int) zPos;
    }

    public void setSignOwner() {
        Sign sign = (Sign) Bukkit.getWorld(world).getBlockAt(xPos, yPos, zPos).getState();
        if (sign == null) {
            return;
        }

        sign.setLine(0, Bukkit.getOfflinePlayer(UUID.fromString(owner)).getName());
        sign.update();

        DatabaseManager database = new DatabaseManager();
        database.changeShopOwnerEntry(owner, new Location(Bukkit.getWorld(world), xPos, yPos, zPos));
    }

    public void setSignMode() {
        Sign sign = (Sign) Bukkit.getWorld(world).getBlockAt(xPos, yPos, zPos).getState();
        if (sign == null) {
            return;
        }

        if (mode == 0) {
            sign.setLine(1, "Buying");
            return;
        }
        sign.setLine(1, "Selling");
    }

    public void setSignItem() {
        Sign sign = (Sign) Bukkit.getWorld(world).getBlockAt(xPos, yPos, zPos).getState();
        if (sign == null) {
            return;
        }

        sign.setLine(2, getItem().getItemMeta().getDisplayName());
    }

}
