package xyz.mizarc.physicalshops.guimenu.menus;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import xyz.mizarc.physicalshops.PhysicalShops;
import xyz.mizarc.physicalshops.Shop;
import xyz.mizarc.physicalshops.ShopPurchase;
import xyz.mizarc.physicalshops.guimenu.Menu;
import xyz.mizarc.physicalshops.guimenu.MenuItem;

import java.util.UUID;

public class ShopMenu {
    private Player player;
    private Shop shop;
    private ShopPurchase shopPurchase;
    private Menu menu;
    private Inventory storage;

    public ShopMenu(Player player, Shop shop, Inventory storage) {
        String shopModeTitle = "Shop";
        switch(shop.getMode()) {
            case 0:
                shopModeTitle = "Buying";
                break;
            case 1:
                shopModeTitle = "Selling";
                break;
        }

        this.player = player;
        this.shop = shop;
        this.storage = storage;
        menu = new Menu(player, 9, shopModeTitle);
        shopPurchase = new ShopPurchase(player, shop, storage);
        addMenuItems(shop);
        menu.openInventory();
    }

    private void addMenuItems(Shop shop) {
        ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwningPlayer(Bukkit.getOfflinePlayer(UUID.fromString(shop.getOwner())));
        skull.setItemMeta(meta);

        MenuItem decreaseButton;
        if (canDecrease(2)) {
            decreaseButton = new MenuItem(new ItemStack(Material.RED_CONCRETE),
                    inventoryClickEvent -> decreaseQuantity(2), "§cDecrement -2x");
        }
        else {
            decreaseButton = new MenuItem(new ItemStack(Material.GRAY_CONCRETE),
                    "§8Cannot decrement further");
        }

        MenuItem increaseButton;
        if (canIncrease(2)) {
            increaseButton = new MenuItem(new ItemStack(Material.GREEN_CONCRETE),
                    inventoryClickEvent -> increaseQuantity(2), "§aIncrement 2x");
        }
        else {
            increaseButton = new MenuItem(new ItemStack(Material.GRAY_CONCRETE),
                    "§8Cannot increment further");
        }

        Economy economy = PhysicalShops.getPlugin().getEconomy();

        MenuItem shopItemButton = null;
        String modeFormatted = "";
        String stockFormatted = "";
        if (shop.getMode() == 0) {
            modeFormatted = "Give";
            stockFormatted = shopPurchase.getEmptyInStorage(shop.getItem()) + " available to give";
            shopItemButton = new MenuItem(shop.getItem(), inventoryClickEvent -> shopPurchase.buyItem(),
                    shop.getItem().getItemMeta().getDisplayName(),
                    modeFormatted + " " + shop.getQuantity() * shopPurchase.getMultiplier() + " for " +
                            economy.format(shop.getPrice() * shopPurchase.getMultiplier()));
        }
        else if (shop.getMode() == 1) {
            modeFormatted = "Take";
            stockFormatted = shopPurchase.getItemsInStorage(shop.getItem()) + " available";
            shopItemButton = new MenuItem(shop.getItem(), inventoryClickEvent -> shopPurchase.sellItem(),
                    shop.getItem().getItemMeta().getDisplayName(),
                    modeFormatted + " " + shop.getQuantity() * shopPurchase.getMultiplier() + " for " +
                            economy.format(shop.getPrice() * shopPurchase.getMultiplier()));
        }

        double playerBalance = economy.getBalance(Bukkit.getOfflinePlayer(player.getUniqueId()));
        double shopOwnerBalance = economy.getBalance(Bukkit.getOfflinePlayer(UUID.fromString(shop.getOwner())));

        menu.addMenuItem(0, decreaseButton);
        menu.addMenuItem(1, shopItemButton);
        menu.addMenuItem(2, increaseButton);

        menu.addMenuItem(4, new MenuItem(new ItemStack(Material.CHEST), "§bStock Levels", stockFormatted));
        menu.addMenuItem(5, new MenuItem(new ItemStack(Material.GOLD_INGOT), "§6Your Gold",
                economy.format(playerBalance)));
        menu.addMenuItem(6, new MenuItem(new ItemStack(skull), "§6Their Gold",
                economy.format(shopOwnerBalance)));

        menu.addMenuItem(8, new MenuItem(new ItemStack(Material.NETHER_STAR),
                inventoryClickEvent -> menu.closeInventory(), "§4Close"));
    }

    private void increaseQuantity(int multiplier) {
        if (canIncrease(multiplier)) {
            shopPurchase.setMultiplier(shopPurchase.getMultiplier() * multiplier);
            addMenuItems(shop);
        }
    }

    private void decreaseQuantity(int multiplier) {
        if (canDecrease(multiplier)) {
            shopPurchase.setMultiplier(shopPurchase.getMultiplier() / multiplier);
            addMenuItems(shop);
        }
    }

    private boolean canIncrease(int multiplier) {
        if (shop.getMode() == 0) {
            return shopPurchase.canBuy(shopPurchase.getMultiplier() * multiplier);
        }

        return shopPurchase.canSell(shopPurchase.getMultiplier() * multiplier);
    }

    private boolean canDecrease(int multiplier) {
        return shopPurchase.getMultiplier() / multiplier >= 1;
    }
}