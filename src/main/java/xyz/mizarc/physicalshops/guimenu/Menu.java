package xyz.mizarc.physicalshops.guimenu;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import xyz.mizarc.physicalshops.guimenu.events.MenuCloseEvent;
import xyz.mizarc.physicalshops.guimenu.events.MenuOpenEvent;

public class Menu {
    private Player player;
    private Inventory inv;
    private MenuItem[] menuItems = new MenuItem[9];

    public Menu(Player player, int size, String title) {
        this.player = player;
        inv = Bukkit.createInventory(null, size, title);
    }

    public Inventory getInventory() {
        return inv;
    }

    public void openInventory() {
        applyItems();
        player.openInventory(inv);
        MenuOpenEvent event = new MenuOpenEvent(this, inv);
        Bukkit.getPluginManager().callEvent(event);
    }

    public void closeInventory() {
        player.closeInventory();
        MenuCloseEvent event = new MenuCloseEvent(this, inv);
        Bukkit.getPluginManager().callEvent(event);
    }

    public void appendMenuItem(MenuItem menuItem) {
        for (int i=0; i < menuItems.length; i++) {
            if (menuItems[i] != null) {
                menuItems[i] = menuItem;
            }
        }
    }

    public MenuItem getMenuItem(int index) {
        return menuItems[index];
    }

    public void addMenuItem(int index, MenuItem menuItem) {
        menuItems[index] = menuItem;
    }

    public void removeMenuItem(int index) {
        menuItems[index] = null;
    }

    public void applyItems() {
        for (int i=0; i < menuItems.length; i++) {
            if (menuItems[i] == null) {
                continue;
            }
            inv.setItem(i, menuItems[i].getItem());
        }
    }
}
