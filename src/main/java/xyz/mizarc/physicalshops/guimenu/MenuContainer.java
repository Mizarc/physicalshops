package xyz.mizarc.physicalshops.guimenu;

import org.bukkit.inventory.Inventory;

import java.util.HashMap;
import java.util.Map;

public class MenuContainer {
    private Map<Inventory, Menu> loadedMenus = new HashMap();

    public Menu getMenu(Inventory inventory) {
        return loadedMenus.get(inventory);
    }

    public void addMenu(Inventory inventory, Menu menu) {
        loadedMenus.put(inventory, menu);
    }

    public void removeMenu(Inventory inventory) {
        loadedMenus.remove(inventory);
    }
}
