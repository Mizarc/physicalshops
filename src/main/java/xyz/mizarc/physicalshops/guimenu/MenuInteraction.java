package xyz.mizarc.physicalshops.guimenu;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import xyz.mizarc.physicalshops.PhysicalShops;
import xyz.mizarc.physicalshops.guimenu.events.MenuCloseEvent;
import xyz.mizarc.physicalshops.guimenu.events.MenuOpenEvent;

public class MenuInteraction implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Menu menu = PhysicalShops.getPlugin().getMenuContainer().getMenu(event.getClickedInventory());
        if (menu == null) {
            return;
        }

        event.setCancelled(true);

        MenuItem menuItem = menu.getMenuItem(event.getSlot());
        if (menuItem == null) {
            return;
        }
        if (!menuItem.isAction()) {
            return;
        }
        menuItem.runAction(event);
        menu.applyItems();
    }

    @EventHandler
    public void onMenuOpen(MenuOpenEvent event) {
        MenuContainer container = PhysicalShops.getPlugin().getMenuContainer();
        container.addMenu(event.getInventory(), event.getMenu());
    }

    @EventHandler
    public void onMenuClose(MenuCloseEvent event) {
        MenuContainer container = PhysicalShops.getPlugin().getMenuContainer();
        container.removeMenu(event.getInventory());
    }
}
