package xyz.mizarc.physicalshops.guimenu;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class MenuItem {
    private ItemStack item;
    private Consumer<InventoryClickEvent> action;

    public MenuItem(ItemStack item) {
        item = this.item;
    }

    public MenuItem(ItemStack item, Consumer<InventoryClickEvent> action) {
        item = this.item;
        this.action = action;
    }

    public MenuItem(ItemStack item, String name, String... lores) {
        this.item = item;
        ItemMeta meta = item.getItemMeta();


        if (name.contains("§")) {
            meta.setDisplayName("§f" + name);
        } else {
            meta.setDisplayName(name);
        }

        List<String> formattedLore = new ArrayList<String>();
        for (String lore : lores) {
            if (lore.contains("§")) {
                formattedLore.add(lore);
                continue;
            }
            formattedLore.add("§5" + lore);
        }
        meta.setLore(formattedLore);

        item.setItemMeta(meta);
    }

    public MenuItem(ItemStack item, Consumer<InventoryClickEvent> action, String name, String... lores) {
        this.item = item;
        ItemMeta meta = item.getItemMeta();

        if (name.contains("§")) {
            meta.setDisplayName("§f" + name);
        } else {
            meta.setDisplayName(name);
        }

        List<String> formattedLore = new ArrayList<String>();
        for (String lore : lores) {
            if (lore.contains("§")) {
                formattedLore.add(lore);
                continue;
            }
            formattedLore.add("§5" + lore);
        }
        meta.setLore(formattedLore);

        item.setItemMeta(meta);
        this.action = action;
    }

    public ItemStack getItem() {
        return item;
    }

    public Consumer<InventoryClickEvent> getAction() {
        return action;
    }

    public void setItem(ItemStack item) {

    }

    public void setName(String name) {
        ItemMeta meta = this.item.getItemMeta();
        meta.setDisplayName(name);
        this.item.setItemMeta(meta);
    }

    public void setLore(String... lore) {
        ItemMeta meta = this.item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        this.item.setItemMeta(meta);
    }

    public boolean isAction() {
        return this.action != null;
    }

    public void setAction(Consumer<InventoryClickEvent> action) {
        this.action = action;
    }

    public void runAction(InventoryClickEvent event) {
        action.accept(event);
    }
}
