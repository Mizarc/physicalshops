package xyz.mizarc.physicalshops;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.UUID;

public class ShopPurchase {
    private Player player;
    private Shop shop;
    private Inventory storage;
    private int multiplier;

    public ShopPurchase(Player player, Shop shop, Inventory storage) {
        this.player = player;
        this.shop = shop;
        this.storage = storage;
        this.multiplier = 1;
    }

    public void buyItem() {
        if (!canBuy()) {
            return;
        }

        Economy economy = PhysicalShops.getPlugin().getEconomy();
        ItemStack item = shop.getItem();
        item.setAmount(shop.getQuantity() * multiplier);
        economy.withdrawPlayer(Bukkit.getOfflinePlayer(
                UUID.fromString(shop.getOwner())), shop.getPrice() * multiplier);
        economy.depositPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()), shop.getPrice() * multiplier);
        storage.addItem(item);
        player.getInventory().removeItem(item);
    }

    public void sellItem() {
        if (!canSell()) {
            return;
        }

        Economy economy = PhysicalShops.getPlugin().getEconomy();
        ItemStack item = shop.getItem();
        item.setAmount(shop.getQuantity() * multiplier);
        economy.withdrawPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()), shop.getPrice() * multiplier);
        economy.depositPlayer(Bukkit.getOfflinePlayer(
                UUID.fromString(shop.getOwner())), shop.getPrice() * multiplier);
        storage.removeItem(item);

        int freeSpace = 0;
        for (ItemStack invSlot : player.getInventory()) {
            if (invSlot == null) {
                freeSpace += item.getMaxStackSize();
                continue;
            }
            if (invSlot.isSimilar(item)) {
                freeSpace += invSlot.getMaxStackSize() - invSlot.getAmount();
            }
        }

        if (shop.getQuantity() * multiplier < freeSpace) {
            player.getInventory().addItem(item);
            return;
        }

        item.setAmount(freeSpace);
        player.getInventory().addItem(item);
        item.setAmount((shop.getQuantity() * multiplier) - freeSpace);
        player.getWorld().dropItem(player.getLocation(), item);
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public boolean canBuy() {
        Economy economy = PhysicalShops.getPlugin().getEconomy();
        boolean quantityCheck = shop.getQuantity() * multiplier <= getEmptyInStorage(shop.getItem());
        boolean priceCheck = shop.getPrice() * multiplier <=
                economy.getBalance(Bukkit.getOfflinePlayer(UUID.fromString(shop.getOwner())));

        return quantityCheck && priceCheck;
    }

    public boolean canBuy(int testMultiplier) {
        Economy economy = PhysicalShops.getPlugin().getEconomy();
        boolean quantityCheck = shop.getQuantity() * testMultiplier <= getEmptyInStorage(shop.getItem());
        boolean priceCheck = shop.getPrice() * testMultiplier <=
                economy.getBalance(Bukkit.getOfflinePlayer(UUID.fromString(shop.getOwner())));

        return quantityCheck && priceCheck;
    }

    public boolean canSell() {
        Economy economy = PhysicalShops.getPlugin().getEconomy();
        boolean quantityCheck = shop.getQuantity() * multiplier <= getItemsInStorage(shop.getItem());
        boolean priceCheck = shop.getPrice() * multiplier <=
                economy.getBalance(Bukkit.getOfflinePlayer(player.getUniqueId()));

        return quantityCheck && priceCheck;
    }

    public boolean canSell(int testMultiplier) {
        Economy economy = PhysicalShops.getPlugin().getEconomy();
        boolean quantityCheck = shop.getQuantity() * testMultiplier <= getItemsInStorage(shop.getItem());
        boolean priceCheck = shop.getPrice() * testMultiplier <=
                economy.getBalance(Bukkit.getOfflinePlayer(player.getUniqueId()));

        return quantityCheck && priceCheck;
    }

    public int getEmptyInStorage(ItemStack item) {
        int freeSpace = 0;
        Inventory inventory = storage;

        for (ItemStack invItem : inventory) {
            if (invItem == null) {
                freeSpace += item.getMaxStackSize();
            } else if (invItem.isSimilar(item)) {
                freeSpace += invItem.getMaxStackSize() - invItem.getAmount();
            }
        }

        return freeSpace;
    }

    public int getItemsInStorage(ItemStack item) {
        int itemAmount = 0;
        Inventory inventory = storage;

        for (ItemStack invItem : inventory) {
            if (invItem == null) {
                continue;
            }
            if (invItem.isSimilar(item)) {
                itemAmount += invItem.getAmount();
            }
        }

        return itemAmount;
    }
}
